//
//  MenuViewController.swift
//  TotalplaySAF
//
//  Created by fer on 05/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var avanceProgressView: UIProgressView!
    
    @IBOutlet weak var leadingConstraint:
    NSLayoutConstraint!
    
    var menuShowing = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceProgressView.setProgress(self.avanceProgressView.progress + 10, animated: true)
            
            if self.avanceProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    

    @IBAction func openMenu(_ sender: Any) {
        
        if (menuShowing) {
            leadingConstraint.constant = -140
        }else{
             leadingConstraint.constant = 0
            
            UIView.animate(withDuration: 01,
                           animations: {
                             self.view.layoutIfNeeded()
            })
        }
        menuShowing = !menuShowing
    }
}
