//
//  CapitalHumanViewController.swift
//  TotalplaySAF
//
//  Created by fer on 10/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CapitalHumanViewController: UIViewController {

    @IBOutlet weak var avanceCapitalProgressView: UIProgressView!
    
    @IBOutlet weak var avanceCap2ProgressView: UIProgressView!
    @IBOutlet weak var avanceCap3ProgressView: UIProgressView!
    @IBOutlet weak var avanceCap4ProgressView: UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceCapitalProgressView.setProgress(self.avanceCapitalProgressView.progress + 10, animated: true)
            
            if self.avanceCapitalProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        
        let timer2 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceCap2ProgressView.setProgress(self.avanceCap2ProgressView.progress + 10, animated: true)
            
            if self.avanceCap2ProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        
        let timer3 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceCap3ProgressView.setProgress(self.avanceCap3ProgressView.progress + 10, animated: true)
            
            if self.avanceCap3ProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        
        let timer4 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceCap4ProgressView.setProgress(self.avanceCap4ProgressView.progress + 10, animated: true)
            
            if self.avanceCap4ProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
